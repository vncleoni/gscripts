function deleteRowsWithZeroInColumn() {
  var sheet = SpreadsheetApp.getActiveSpreadsheet().getActiveSheet();
  var columnNumber = 5; // Altere este número para o número da coluna que você quer verificar (1 para a coluna A, 2 para a coluna B, etc.)
  var lastRow = sheet.getLastRow();
  var range = sheet.getRange(1, columnNumber, lastRow, 1);
  var values = range.getValues();
  
  for (var i = values.length - 1; i >= 0; i--) {
    if (values[i][0] == 0) {
      sheet.deleteRow(i + 1);
    }
  }
}
